#!/usr/bin/env python3

import time

origin = 1372629600 #17:00 Jun 30 2013

current = round(time.time())
y = 31536000
d = 86400
h = 3600
m = 60
running = True

msg = 'your message here' #Replace with your message

def calc():
    current = round(time.time())
    elap = current - origin
    years = int(elap /y) 
    days = int((elap % y) /d)
    hours = int((elap % d) / h)
    minutes = int((elap % h) / m)
    seconds = int(elap % m)
    counter = "{} Years {} Days {} Hours {} Minutes {} Seconds {}".format(years, days, hours, minutes, seconds, msg)
    print(counter)

while running:
    calc()    
    time.sleep(1)

