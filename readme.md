# elapsed-time.py

- Calculates the elapsed time from a given point in time to the second.
- Displays the elapsed time with custom message to the terminal every second

### Finding your starting time in epoch format
#### Linux
Run the following command from your terminal:

````bash
date -d 'TZ="America/Chicago" 00:00 Apr 05 2017' +%s
````

### Customizing the message
Edit the `msg` variable in the script
